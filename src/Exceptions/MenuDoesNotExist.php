<?php

namespace CyberExploits\Permission\Exceptions;

use InvalidArgumentException;

class MenuDoesNotExist extends InvalidArgumentException
{
    public static function create(string $menuName)
    {
        return new static("There is no menu named `{$menuName}`.");
    }
}

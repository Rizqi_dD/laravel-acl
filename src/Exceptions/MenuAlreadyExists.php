<?php

namespace CyberExploits\Permission\Exceptions;

use InvalidArgumentException;

class MenuAlreadyExists extends InvalidArgumentException
{
    public static function create(string $menuName, string $guardName)
    {
        return new static("A `{$menuName}` menu already exists for guard `{$guardName}`.");
    }
}

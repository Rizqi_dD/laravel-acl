<?php

namespace CyberExploits\Permission\Traits;

use Illuminate\Support\Collection;
use CyberExploits\Permission\Contracts\Role;
use CyberExploits\Permission\Traits\HasMenus;
use CyberExploits\Permission\Contracts\Menu;
use CyberExploits\Permission\Contracts\Permission;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\MorphToMany;

trait HasRoles
{
    use HasMenus;

    /**
     * A model may have multiple roles.
     */
    public function roles(): MorphToMany
    {
        return $this->morphToMany(
            config('permission.models.role'),
            'model',
            config('permission.table_names.model_has_roles'),
            'model_id',
            'role_id'
        );
    }

    /**
     * A model may have multiple direct permissions.
     */
    public function permissions(): MorphToMany
    {
        return $this->morphToMany(
            config('permission.models.permission'),
            'model',
            config('permission.table_names.model_has_permissions'),
            'model_id',
            'permission_id'
        );
    }

     /**
     * A model may have multiple direct permissions.
     */
    public function menus(): MorphToMany
    {
        return $this->morphToMany(
            config('permission.models.menu'),
            'model',
            config('permission.table_names.model_has_menus'),
            'model_id',
            'menu_id'
        );
    }

    /**
     * Scope the model query to certain roles only.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string|array|Role|\Illuminate\Support\Collection $roles
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeRole(Builder $query, $roles): Builder
    {
        if ($roles instanceof Collection) {
            $roles = $roles->toArray();
        }

        if (! is_array($roles)) {
            $roles = [$roles];
        }

        $roles = array_map(function ($role) {
            if ($role instanceof Role) {
                return $role;
            }

            return app(Role::class)->findByName($role, $this->getDefaultGuardName());
        }, $roles);

        return $query->whereHas('roles', function ($query) use ($roles) {
            $query->where(function ($query) use ($roles) {
                foreach ($roles as $role) {
                    $query->orWhere(config('permission.table_names.roles').'.id', $role->id);
                }
            });
        });
    }

    /**
     * Assign the given role to the model.
     *
     * @param array|string|\CyberExploits\Permission\Contracts\Role ...$roles
     *
     * @return $this
     */
    public function assignRole(...$roles)
    {
        $roles = collect($roles)
            ->flatten()
            ->map(function ($role) {
                return $this->getStoredRole($role);
            })
            ->each(function ($role) {
                $this->ensureModelSharesGuard($role);
            })
            ->all();

        $this->roles()->saveMany($roles);

        $this->forgetCachedMenus();
        $this->forgetCachedPermissions();

        return $this;
    }

    /**
     * Revoke the given role from the model.
     *
     * @param string|\CyberExploits\Permission\Contracts\Role $role
     */
    public function removeRole($role)
    {
        $this->roles()->detach($this->getStoredRole($role));
    }

    /**
     * Remove all current roles and set the given ones.
     *
     * @param array ...$roles
     *
     * @return $this
     */
    public function syncRoles(...$roles)
    {
        $this->roles()->detach();

        return $this->assignRole($roles);
    }
    /**
     * Determine if the model has (one of) the given role(s).
     *
     * @param string|array|\CyberExploits\Permission\Contracts\Role|\Illuminate\Support\Collection $roles
     *
     * @return bool
     */
    public function hasRole($roles): bool
    {
        
        if (is_string($roles) && false !== strpos($roles, '|')) {
            $roles = $this->convertPipeToArray($roles);
        }
        if (is_string($roles)) {
            return $this->roles->contains('name', $roles);
        }
        if ($roles instanceof Role) {
            return $this->roles->contains('id', $roles->id);
        }
        if (is_array($roles)) {
            foreach ($roles as $role) {
                if ($this->hasRole($role)) {
                    return true;
                }
            }
            return false;
        }
        return $roles->intersect($this->roles)->isNotEmpty();
    }
    
    /**
     * Determine if the model has (one of) the given role(s).
     *
     * @param string|array|\CyberExploits\Permission\Contracts\Role|\Illuminate\Support\Collection $roles
     *
     * @return bool
     */
    public function hasPermission($permissions): bool
    {
        if (is_string($permissions)) {
            return $this->permissions->contains('name', $permissions);
        }

        if ($permissions instanceof Role) {
            return $this->permissions->contains('id', $permissions->id);
        }

        if (is_array($permissions)) {
            foreach ($permissions as $permission) {
                if ($this->hasPermission($permission)) {
                    return true;
                }
            }
            return false;
        }

        return $permissions->intersect($this->permissions)->isNotEmpty();
    }
    /**
     * Determine if the model has any of the given role(s).
     *
     * @param string|array|\CyberExploits\Permission\Contracts\Role|\Illuminate\Support\Collection $roles
     *
     * @return bool
     */
    public function hasAnyRole($roles): bool
    {
        return $this->hasRole($roles);
    }

    /**
     * Determine if the model has all of the given role(s).
     *
     * @param string|\CyberExploits\Permission\Contracts\Role|\Illuminate\Support\Collection $roles
     *
     * @return bool
     */
    public function hasAllRoles($roles): bool
    {
        if (is_string($roles) && false !== strpos($roles, '|')) {
            $roles = $this->convertPipeToArray($roles);
        }
        if (is_string($roles)) {
            return $this->roles->contains('name', $roles);
        }
        if ($roles instanceof Role) {
            return $this->roles->contains('id', $roles->id);
        }
        $roles = collect()->make($roles)->map(function ($role) {
            return $role instanceof Role ? $role->name : $role;
        });

        return $roles->intersect($this->roles->pluck('name')) == $roles;
    }

    /**
     * Determine if the model may perform the given permission.
     *
     * @param string|\CyberExploits\Permission\Contracts\Permission $permission
     *
     * @return bool
     */
    public function hasPermissionTo($menus,$permissions): bool
    {
        if (is_array($menus) && is_array($permissions)) {
            foreach ($menus as $menu) {
                $menu = app(Permission::class)->findByName($menu,$this->getDefaultGuardName());
            }
            foreach ($permissions as $permission) {
                $permission = app(Menu::class)->findByName($permission,$this->getDefaultGuardName());
            }
            return false;
        }
        
        if (is_string($permissions)) {
            $permissions = app(Permission::class)->findByName($permissions,$this->getDefaultGuardName());
        }
        
        if (is_string($menus)) {
            $menus = app(Menu::class)->findByName($menus,$this->getDefaultGuardName());
        }
        return $this->hasDirectPermission($menus,$permissions) && $this->hasPermissionViaRole($menus,$permissions);
    }
    
    /**
     * Determine if the model may perform the given permission.
     *
     * @param string|\CyberExploits\Permission\Contracts\Permission $permission
     *
     * @return bool
     */
    public function hasMenuTo($menu): bool
    {
        if (is_string($menu)) {
            $menu = app(Menu::class)->findByName($menu,$this->getDefaultGuardName());
        } 
        return $this->hasDirectMenu($menu) || $this->hasMenuViaRole($menu);
    }

    /**
     * Determine if the model has any of the given permissions.
     *
     * @param array ...$permissions
     *
     * @return bool
     */
    public function hasAnyPermission($menus,$permissions): bool
    {
        foreach ($menus as $menu) {
            foreach ($permissions as $permission) {
                if ($this->hasPermissionTo($menu,$permission)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Determine if the model has any of the given permissions.
     *
     * @param array ...$permissions
     *
     * @return bool
     */
    public function hasAnyMenu($menus): bool
    {
        foreach ($menus as $menu) {
            if ($this->hasMenuTo($menu)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Determine if the model has, via roles, the given permission.
     *
     * @param \CyberExploits\Permission\Contracts\Permission $permission
     *
     * @return bool
     */
    protected function hasPermissionViaRole($permission, $menu): bool
    {
        return $this->hasRole($permission->roles) && $this->hasRole($menu->roles);
    }

    /**
     * Determine if the model has, via roles, the given permission.
     *
     * @param \CyberExploits\Permission\Contracts\Permission $permission
     *
     * @return bool
     */
    protected function hasMenuViaRole(Menu $menu): bool
    {
        return $this->hasRole($menu->roles);
    }

    /**
     * Determine if the model has the given permission.
     *
     * @param string|\CyberExploits\Permission\Contracts\Permission $permission
     *
     * @return bool
     */
    public function hasDirectPermission($menu, $permission): bool
    {
        if (is_string($permission)) {
            $permission = app(Permission::class)->findByName($permission,$this->getDefaultGuardName());
            if (! $permission) {
                return false;
            }
        }
        return $menu->permissions->where('pivot.role_id',$this->roles()->get()->pluck('id')->implode(' '))->contains('id', $permission->id);
    }

    /**
     * Determine if the model has the given permission.
     *
     * @param string|\CyberExploits\Permission\Contracts\Permission $permission
     *
     * @return bool
     */
    public function hasDirectMenu($menu): bool
    {
        if (is_string($menu)) {
            $menu = app(Menu::class)->findByName($menu, $this->getDefaultGuardName());
            if (! $menu) {
                return false;
            }
        }
        return $this->menus->contains('id', $menu->id);
    }

    /**
     * Return all permissions the directory coupled to the model.
     */
    public function getDirectPermissions(): Collection
    {
        return $this->permissions;
    }

    /**
     * Return all permissions the directory coupled to the model.
     */
    public function getDirectMenus(): Collection
    {
        return $this->menus;
    }

    /**
     * Return all the permissions the model has via roles.
     */
    public function getPermissionsViaMenus(): Collection
    {
        return $this->load('roles', 'roles.menus.permissions')
                ->roles->flatMap(function ($role) {
                    return $role->menus;
                })->sort()->values()->flatMap(function ($menu) {
                    return $menu->permissions;
                })->sort()->values();
    }

    /**
     * Return all the permissions the model has via roles.
     */
    public function getMenusViaRoles(): Collection
    {
        return $this->load('roles', 'roles.menus')
            ->roles->flatMap(function ($role) {
                return $role->menus;
            })->sort()->values();
    }

    /**
     * Return all the permissions the model has, both directly and via roles.
     */
    public function getAllPermissions(): Collection
    {
        return $this->permissions
            ->merge($this->getPermissionsViaMenus())
            ->sort()
            ->values();
    }

     /**
     * Return all the permissions the model has, both directly and via roles.
     */
    public function getAllMenus(): Collection
    {
        return $this->menus
            ->merge($this->getMenusViaRoles())
            ->sort()
            ->values();
    }

    /**
     * Return all the permissions the model has, both directly and via roles.
     */
    protected function getStoredRole($role): Role
    {
        if (is_string($role)) {
            return app(Role::class)->findByName($role,$this->getDefaultGuardName());
        }
        return $role;
    }

    protected function convertPipeToArray(string $pipeString)
    {
        $pipeString = trim($pipeString);
        if (strlen($pipeString) <= 2) {
            return $pipeString;
        }
        $quoteCharacter = substr($pipeString, 0, 1);
        $endCharacter = substr($quoteCharacter, -1, 1);
        if ($quoteCharacter !== $endCharacter) {
            return explode('|', $pipeString);
        }
        if (! in_array($quoteCharacter, ["'", '"'])) {
            return explode('|', $pipeString);
        }
        return explode('|', trim($pipeString, $quoteCharacter));
    }
}

<?php

namespace CyberExploits\Permission\Traits;

use Illuminate\Support\Collection;
use CyberExploits\Permission\MenuRegistrar;
use CyberExploits\Permission\Contracts\Menu;
use CyberExploits\Permission\Traits\HasPermissions;
use CyberExploits\Permission\Exceptions\GuardDoesNotMatch;

trait HasMenus
{
    use HasPermissions;
    /**
     * Grant the given permission(s) to a role.
     *
     * @param string|array|\CyberExploits\Permission\Contracts\Permission|\Illuminate\Support\Collection $permissions
     *
     * @return $this
     */
    public function giveMenuTo($menus,$permissions)
    {
        $this->permissions()->attach($permissions->id,['menu_id' => $menus->id]);
        $this->forgetCachedMenus();
        return $this;
    }

    /**
     * Remove all current permissions and set the given ones.
     *
     * @param string|array|\CyberExploits\Permission\Contracts\Permission|\Illuminate\Support\Collection $permissions
     *
     * @return $this
     */
    public function syncMenus(...$menus)
    {
        $this->menus()->detach();

        return $this->giveMenuTo($menus);
    }

    /**
     * Revoke the given permission.
     *
     * @param \CyberExploits\Permission\Contracts\Permission|string $permission
     *
     * @return $this
     */
    public function revokeMenuTo($menu,$permission)
    {
        $this->permissions()->detach($permission->id,['menu_id' => $menu->id]);

        $this->forgetCachedMenus();

        return $this;
    }

    /**
     * @param string|array|\CyberExploits\Permission\Contracts\Permission|\Illuminate\Support\Collection $permissions
     *
     * @return \CyberExploits\Permission\Contracts\Permission
     */
    protected function getStoredMenu($menus): Menu
    {
        if (is_string($menus)) {
            return app(Menu::class)->findByName($menus, $this->getDefaultGuardName());
        }

        if (is_array($menus)) {
            return app(Menu::class)
                ->whereIn('name', $menus)
                ->whereId('guard_name', $this->getGuardNames())
                ->get();
        }
        return $menus;
    }

    /**
     * @param \CyberExploits\Permission\Contracts\Permission|\CyberExploits\Permission\Contracts\Role $roleOrPermission
     *
     * @throws \CyberExploits\Permission\Exceptions\GuardMismatch
     */
    protected function ensureModelSharesGuard($roleOrMenuOrPermission)
    {
        if (! $this->getGuardNames()->contains($roleOrMenuOrPermission->guard_name)) {
            throw GuardDoesNotMatch::create($roleOrMenuOrPermission->guard_name, $this->getGuardNames());
        }
    }

    protected function getGuardNames(): Collection
    {
        if ($this->guard_name) {
            return collect($this->guard_name);
        }

        return collect(config('auth.guards'))
            ->map(function ($guard) {
                return config("auth.providers.{$guard['provider']}.model");
            })
            ->filter(function ($model) {
                return get_class($this) === $model;
            })
            ->keys();
    }

    protected function getDefaultGuardName(): string
    {
        $default = config('auth.defaults.guard');
        return $this->getGuardNames()->only([0,2]) ?: $default;
    }

    /**
     * Forget the cached permissions.
     */
    public function forgetCachedMenus()
    {
        app(MenuRegistrar::class)->forgetCachedMenus();
    }
}

<?php

namespace CyberExploits\Permission\Traits;

use Illuminate\Database\Eloquent\Model;
use CyberExploits\Permission\MenuRegistrar;

trait RefreshesMenuCache
{
    public static function bootRefreshesMenuCache()
    {
        static::created(function (Model $model) {
            app(MenuRegistrar::class)->forgetCachedMenus();
        });

        static::updated(function (Model $model) {
            app(MenuRegistrar::class)->forgetCachedMenus();
        });

        static::deleted(function (Model $model) {
            app(MenuRegistrar::class)->forgetCachedMenus();
        });
    }
}

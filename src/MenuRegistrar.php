<?php

namespace CyberExploits\Permission;

use Exception;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Illuminate\Contracts\Auth\Access\Gate;
use Illuminate\Contracts\Cache\Repository;
use CyberExploits\Permission\Contracts\Menu;

class MenuRegistrar
{
    /** @var \Illuminate\Contracts\Auth\Access\Gate */
    protected $gate;

    /** @var \Illuminate\Contracts\Cache\Repository */
    protected $cache;

    /** @var Illuminate\Contracts\Logging\Log */
    protected $logger;

    /** @var string */
    # php artisan cache:forget cyberexploits.permission.cacheMenu
    protected $cacheKey = 'cyberexploits.permission.cacheMenu';

    public function __construct(Gate $gate, Repository $cache, Log $logger)
    {
        $this->gate = $gate;
        $this->cache = $cache;
        $this->logger = $logger;
    }

    public function registerMenus(): bool
    {
        try {
            $this->getMenus()->map(function ($menu) {
                $this->gate->define($menu->name, function ($user) use ($menu) {
                    return $user->hasMenuTo($menu);
                });
            });
            return true;
        } catch (Exception $exception) {
            if ($this->shouldLogException()) {
                $this->logger->alert(
                    "Could not register menus because {$exception->getMessage()}".PHP_EOL.
                    $exception->getTraceAsString()
                );
            }

            return false;
        }
    }

    public function forgetCachedMenus()
    {
        $this->cache->forget($this->cacheKey);
    }

    public function getMenus(): Collection
    {
        return $this->cache->remember($this->cacheKey, config('permission.cache_expiration_time'), function () {
            return app(Menu::class)->with('roles')->get();
        });
    }

    protected function shouldLogException(): bool
    {
        return config('permission.log_registration_exception');
    }
}

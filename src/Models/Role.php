<?php

namespace CyberExploits\Permission\Models;

use Illuminate\Database\Eloquent\Model;
use CyberExploits\Permission\Traits\HasMenus;
use CyberExploits\Permission\Exceptions\RoleDoesNotExist;
use CyberExploits\Permission\Exceptions\GuardDoesNotMatch;
use CyberExploits\Permission\Exceptions\RoleAlreadyExists;
use CyberExploits\Permission\Contracts\Role as RoleContract;
use CyberExploits\Permission\Traits\RefreshesPermissionCache;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\TripleBelongsToMany;

class Role extends Model implements RoleContract
{
    use HasMenus;
    use RefreshesPermissionCache;

    public $guarded = ['id'];

    public function __construct(array $attributes = [])
    {
        $attributes['guard_name'] = $attributes['guard_name'] ?? config('auth.defaults.guard');

        parent::__construct($attributes);

        $this->setTable(config('permission.table_names.roles'));
    }

    public static function create(array $attributes = [])
    {
        $attributes['guard_name'] = $attributes['guard_name'] ?? config('auth.defaults.guard');

        if (static::where('name', $attributes['name'])->where('guard_name', $attributes['guard_name'])->first()) {
            throw RoleAlreadyExists::create($attributes['name'], $attributes['guard_name']);
        }

        return static::query()->create($attributes);
    }

    /**
     * A role may be given various permissions.
     */
    public function permissions(): BelongsToMany
    {
        return $this->belongsToMany(
            config('permission.models.permission'),
            config('permission.table_names.roles_menus_permissions')
        )->withPivot('menu_id');
    }
    /**
     * A role may be given various permissions.
     */
    public function menus(): BelongsToMany
    {
        return $this->belongsToMany(
            config('permission.models.menu'),
            config('permission.table_names.roles_menus_permissions')
        )->withPivot('permission_id');
    }
    /**
     * A role belongs to some users of the model associated with its guard.
     */
    public function users(): MorphToMany
    {
        return $this->morphedByMany(
            getModelForGuard($this->attributes['guard_name']),
            'model',
            config('permission.table_names.model_has_roles'),
            'role_id',
            'model_id'
        );
    }
    /**
     * Find a role by its name and guard name.
     *
     * @param string $name
     * @param string|null $guardName
     *
     * @return \CyberExploits\Permission\Contracts\Role|\CyberExploits\Permission\Models\Role
     *
     * @throws \CyberExploits\Permission\Exceptions\RoleDoesNotExist
     */
    public static function findByName(string $name, $guardName = null): RoleContract
    {
        $guardName = $guardName ? json_decode($guardName) : config('auth.defaults.guard');
        
        $role = static::where('name', $name)->whereIn('guard_name', $guardName)->first();

        if (! $role) {
            throw RoleDoesNotExist::create($name);
        }

        return $role;
    }

    /**
     * Determine if the user may perform the given permission.
     *
     * @param string|Permission $permission
     *
     * @return bool
     *
     * @throws \CyberExploits\Permission\Exceptions\GuardMismatch
     */
    public function hasPermissionTo($permission): bool
    {
        if (is_string($permission)) {
            $permission = app(Permission::class)->findByName($permission, $this->getDefaultGuardName());
        }

        if (! $this->getGuardNames()->contains($permission->guard_name)) {
            throw GuardDoesNotMatch::create($permission->guard_name, $this->getGuardNames());
        }
        return $this->permissions->contains('id', $permission->id);
    }
    /**
     * Determine if the user may perform the given permission.
     *
     * @param string|Permission $permission
     *
     * @return bool
     *
     * @throws \CyberExploits\Permission\Exceptions\GuardMismatch
     */
    public function hasMenuTo($menu): bool
    {
        if (is_string($menu)) {
            $menu = app(Menu::class)->findByName($menu, $this->getDefaultGuardName());
        }

        if (! $this->getGuardNames()->contains($menu->guard_name)) {
            throw GuardDoesNotMatch::create($menu->guard_name, $this->getGuardNames());
        }

        return $this->menus->contains('id', $menu->id);
    }
}

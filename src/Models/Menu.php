<?php

namespace CyberExploits\Permission\Models;

use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use CyberExploits\Permission\MenuRegistrar;
use CyberExploits\Permission\Traits\HasPermissions;
use CyberExploits\Permission\Traits\RefreshesMenuCache;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use CyberExploits\Permission\Exceptions\MenuDoesNotExist;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use CyberExploits\Permission\Exceptions\MenuAlreadyExists;
use CyberExploits\Permission\Contracts\Menu as MenuContract;

class Menu extends Model implements MenuContract
{
    use HasPermissions;
    use RefreshesMenuCache;

    public $guarded = ['id'];

    public function __construct(array $attributes = [])
    {
        $attributes['guard_name'] = $attributes['guard_name'] ?? config('auth.defaults.guard');

        parent::__construct($attributes);

        $this->setTable(config('permission.table_names.menus'));
    }

    public static function create(array $attributes = [])
    {
        $attributes['guard_name'] = $attributes['guard_name'] ?? config('auth.defaults.guard');

        if (static::getMenus()->where('name', $attributes['name'])->where('guard_name', $attributes['guard_name'])->first()) {
            throw MenuAlreadyExists::create($attributes['name'], $attributes['guard_name']);
        }

        return static::query()->create($attributes);
    }

   /**
     * A role may be given various permissions.
     */
    public function permissions(): BelongsToMany
    {
        return $this->belongsToMany(
            config('permission.models.permission'),
            config('permission.table_names.roles_menus_permissions')
        )->withPivot('role_id');
    }
    
    /**
     * A permission can be applied to roles.
     */
    public function roles(): BelongsToMany
    {
        return $this->belongsToMany(
            config('permission.models.role'),
            config('permission.table_names.roles_menus_permissions')
        )->withPivot('permission_id');
    }

    /**
     * A permission belongs to some users of the model associated with its guard.
     */
    public function users(): MorphToMany
    {
        return $this->morphedByMany(
            getModelForGuard($this->attributes['guard_name']),
            'model',
            config('permission.table_names.model_has_menus'),
            'menu_id',
            'model_id'
        );
    }
    /**
     * Find a permission by its name (and optionally guardName).
     *
     * @param string $name
     * @param string|null $guardName
     *
     * @throws \CyberExploits\Permission\Exceptions\PermissionDoesNotExist
     *
     * @return \CyberExploits\Permission\Contracts\Permission
     */
    public static function findByName(string $name, $guardName = null): MenuContract
    {
        $guardName = $guardName ? json_decode($guardName) : config('auth.defaults.guard');
        
        $menu = static::getMenus()->where('link_name', $name)->whereIn('guard_name', $guardName)->first();

        if (! $menu) {
            throw MenuDoesNotExist::create($name);
        }
        return $menu;
    }

    /**
     * Get the current cached permissions.
     */
    protected static function getMenus(): Collection
    {
        return app(MenuRegistrar::class)->getMenus();
    }
}

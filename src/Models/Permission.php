<?php

namespace CyberExploits\Permission\Models;

use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use CyberExploits\Permission\PermissionRegistrar;
use CyberExploits\Permission\Traits\RefreshesPermissionCache;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use CyberExploits\Permission\Exceptions\PermissionDoesNotExist;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use CyberExploits\Permission\Exceptions\PermissionAlreadyExists;
use CyberExploits\Permission\Contracts\Permission as PermissionContract;

class Permission extends Model implements PermissionContract
{
    use RefreshesPermissionCache;

    public $guarded = ['id'];

    public function __construct(array $attributes = [])
    {
        $attributes['guard_name'] = $attributes['guard_name'] ?? config('auth.defaults.guard');

        parent::__construct($attributes);

        $this->setTable(config('permission.table_names.permissions'));
    }

    public static function create(array $attributes = [])
    {
        $attributes['guard_name'] = $attributes['guard_name'] ?? config('auth.defaults.guard');

        if (static::getPermissions()->where('name', $attributes['name'])->where('guard_name', $attributes['guard_name'])->first()) {
            throw PermissionAlreadyExists::create($attributes['name'], $attributes['guard_name']);
        }

        return static::query()->create($attributes);
    }


    /**
     * A permission can be applied to roles.
     */
    public function menus(): BelongsToMany
    {
        return $this->belongsToMany(
            config('permission.models.menu'),
            config('permission.table_names.roles_menus_permissions')
        )->withPivot('role_id');
    }
    
    /**
     * A permission can be applied to roles.
     */
    public function roles(): BelongsToMany
    {
        return $this->belongsToMany(
            config('permission.models.role'),
            config('permission.table_names.roles_menus_permissions')
        )->withPivot('menu_id');
    }


    /**
     * A permission belongs to some users of the model associated with its guard.
     */
    public function users(): MorphToMany
    {
        return $this->morphedByMany(
            getModelForGuard($this->attributes['guard_name']),
            'model',
            config('permission.table_names.model_has_permissions'),
            'permission_id',
            'model_id'
        );
    }

    /**
     * Find a permission by its name (and optionally guardName).
     *
     * @param string $name
     * @param string|null $guardName
     *
     * @throws \CyberExploits\Permission\Exceptions\PermissionDoesNotExist
     *
     * @return \CyberExploits\Permission\Contracts\Permission
     */
    public static function findByName(string $name, $guardName = null): PermissionContract
    {
        $guardName = $guardName ? json_decode($guardName) : config('auth.defaults.guard');
        
        $permission = static::getPermissions()->where('name', $name)->whereIn('guard_name', $guardName)->first();

        if (! $permission) {
            throw PermissionDoesNotExist::create($name);
        }
        return $permission;
    }

    /**
     * Get the current cached permissions.
     */
    protected static function getPermissions(): Collection
    {   
        return app(PermissionRegistrar::class)->getPermissions();
    }
}

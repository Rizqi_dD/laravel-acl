# Associate users with permissions and roles

[![Latest Version on Packagist](https://img.shields.io/badge/cyberexploits-laravel--acl-green.svg?style=flat-square)](https://packagist.org/packages/cyberexploits/laravel-acl)

This package allows you to manage user permissions and roles in a database.

## Installation

This package can be used in Laravel 5.4 or higher. If you are using an older version of Laravel.

You can install the package via composer:

``` bash
composer require cyber-exploits/laravel-acl
```

Now add the service provider in `config/app.php` file:

```php
'providers' => [
    // ...
    CyberExploits\Permission\PermissionServiceProvider::class,
];
```

You can publish the migration with:

```bash
php artisan vendor:publish --provider="CyberExploits\Permission\PermissionServiceProvider" --tag="migrations"
```

After the migration has been published you can create the role- and permission-tables by
running the migrations:

```bash
php artisan migrate
```

You can publish the config file with:

```bash
php artisan vendor:publish --provider="CyberExploits\Permission\PermissionServiceProvider" --tag="config"
```

## Troubleshooting

### Cache

If you manipulate permission/role data directly in the database instead of calling the supplied methods, then you will not see the changes reflected in the application, because role and permission data is cached to speed up performance.

To manually reset the cache for this package, run:
```bash
php artisan cache:forget cyberexploits.permission.cachePermission
php artisan cache:forget cyberexploits.permission.cacheMenu
```

## About CyberExploits

CyberExploits Team Dev.

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
